<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Guns Table</h2>
                <table border="2px solid black">
                    <tr bgcolor="#f1f1c1">
                        <th>ID</th>
                        <th>Model</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Maximum Firing Range</th>
                        <th>Distance Range</th>
                        <th>Effective Firing Range</th>
                        <th>Has Magazine</th>
                        <th>Has Telescopic Sight</th>
                        <th>Material</th>
                    </tr>
                    <xsl:for-each select="guns/gun">
                        <tr>
                            <td>
                                <xsl:value-of select="@id"/>
                            </td>
                            <td>
                                <xsl:value-of select="model"/>
                            </td>
                            <td>
                                <xsl:value-of select="handy"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="ttc/rangeParameters/maximumFiringRange"/>
                            </td>
                            <td>
                                <xsl:value-of select="ttc/rangeParameters/distanceRange"/>
                            </td>
                            <td>
                                <xsl:value-of select="ttc/rangeParameters/effectiveFiringRange"/>
                            </td>
                            <td>
                                <xsl:value-of select="ttc/hasMagazine"/>
                            </td>
                            <td>
                                <xsl:value-of select="ttc/hasTelescopicSight"/>
                            </td>
                            <td>
                            <xsl:value-of select="material"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>