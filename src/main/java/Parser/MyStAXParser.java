package Parser;

import Model.DistanceRange;
import Model.Gun;
import Model.RangeParameter;
import Model.TTC;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class MyStAXParser {
  List<Gun> gunsList = new ArrayList<>();
  TTC ttc = new TTC();
  Gun gun;

  RangeParameter rangeParameter = new RangeParameter();

  public List<Gun> parseGuns(File xml) {
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    try {
      XMLEventReader xmlEventReader =
          xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "gun":
              gun = new Gun();

              Attribute idAttr = startElement.getAttributeByName(new QName("id"));
              if (idAttr != null) {
                gun.setId(Integer.parseInt(idAttr.getValue()));
              }
              break;
            case "model":
              xmlEvent = xmlEventReader.nextEvent();
              assert gun != null;
              gun.setModel(xmlEvent.asCharacters().getData());
              break;
            case "handy":
              xmlEvent = xmlEventReader.nextEvent();
              assert gun != null;
              gun.setHandy(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "origin":
              xmlEvent = xmlEventReader.nextEvent();
              assert gun != null;
              gun.setOrigin(xmlEvent.asCharacters().getData());
              break;
            case "ttc":
              xmlEvent = xmlEventReader.nextEvent();
              ttc = new TTC();
              break;
            case "rangeParameters":
              xmlEvent = xmlEventReader.nextEvent();
              rangeParameter = new RangeParameter();
              break;
            case "maximumFiringRange":
              xmlEvent = xmlEventReader.nextEvent();
              assert rangeParameter != null;
              rangeParameter.setMaximumFiringRange(
                  Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "distanceRange":
              xmlEvent = xmlEventReader.nextEvent();
              assert rangeParameter != null;
              rangeParameter.setDistanceRange(
                  DistanceRange.valueOf(xmlEvent.asCharacters().getData().toUpperCase()));
              break;
            case "effectiveFiringRange":
              xmlEvent = xmlEventReader.nextEvent();
              assert rangeParameter != null;
              rangeParameter.setEffectiveFiringRange(
                  Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;

            case "hasMagazine":
              xmlEvent = xmlEventReader.nextEvent();
              assert ttc != null;
              ttc.setHasMagazine(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "hasTelescopicSight":
              xmlEvent = xmlEventReader.nextEvent();
              assert ttc != null;
              ttc.setHasTelescopicSight(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "material":
              xmlEvent = xmlEventReader.nextEvent();
              assert gun != null;
              gun.setMaterial(xmlEvent.asCharacters().getData());
              assert rangeParameter != null;
              ttc.setRangeParameter(rangeParameter);
              assert ttc != null;
              gun.setTtc(ttc);
              break;
          }
        }
        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("gun")) {
            gunsList.add(gun);
          }
        }
      }
    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return gunsList;
  }
}
