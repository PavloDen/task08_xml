package Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import Model.DistanceRange;
import Model.Gun;
import Model.RangeParameter;
import Model.TTC;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class MyDOMParser {
  List<Gun> gunsList = new ArrayList<>();
  TTC ttc = new TTC();
  RangeParameter rangeParameter = new RangeParameter();

  public List<Gun> parseGuns(File xml) {
    DOMCreator docCreator = new DOMCreator(xml);
    Document doc = docCreator.getDocument();
    try {
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("gun");
      for (int i = 0; i < nList.getLength(); i++) {
        Node nNode = nList.item(i);
        Gun gun = new Gun();
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          gun.setId(Integer.parseInt(eElement.getAttribute("id")));
          gun.setModel(eElement.getElementsByTagName("model").item(0).getTextContent());
          gun.setHandy(
              Integer.parseInt(eElement.getElementsByTagName("handy").item(0).getTextContent()));
          gun.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
          ttc = getTTC(eElement.getElementsByTagName("ttc"));
          gun.setMaterial(eElement.getElementsByTagName("material").item(0).getTextContent());
          gun.setTtc(ttc);
        }
        gunsList.add(gun);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return gunsList;
  }

  private TTC getTTC(NodeList nodes) {
    TTC ttc = new TTC();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      rangeParameter = getRangeParameter(element.getElementsByTagName("rangeParameters"));
      ttc.setHasMagazine(
          Boolean.parseBoolean(
              element.getElementsByTagName("hasMagazine").item(0).getTextContent()));
      ttc.setHasTelescopicSight(
          Boolean.parseBoolean(
              element.getElementsByTagName("hasTelescopicSight").item(0).getTextContent()));
      ttc.setRangeParameter(rangeParameter);
    }
    return ttc;
  }

  private RangeParameter getRangeParameter(NodeList nodes) {
    RangeParameter rp = new RangeParameter();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      rp.setMaximumFiringRange(
          Integer.parseInt(
              element.getElementsByTagName("maximumFiringRange").item(0).getTextContent()));
      rp.setDistanceRange(
          DistanceRange.valueOf(
              element
                  .getElementsByTagName("distanceRange")
                  .item(0)
                  .getTextContent()
                  .toUpperCase()));
      rp.setEffectiveFiringRange(
          Integer.parseInt(
              element.getElementsByTagName("effectiveFiringRange").item(0).getTextContent()));
    }
    return rp;
  }
}
