package Parser;

import Model.DistanceRange;
import Model.Gun;
import Model.RangeParameter;
import Model.TTC;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MySAXParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public List<Gun> parseGuns(File xmlFile, File xsdFile) {
    List<Gun> gunsList = new ArrayList<>();
    try {
      SchemaFactory schemafactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema sc = schemafactory.newSchema(xsdFile);
      saxParserFactory.setSchema(sc);
      SAXParser parser = saxParserFactory.newSAXParser();
      AdvancedXMLHandler handler = new AdvancedXMLHandler();
      parser.parse(xmlFile, handler);
      gunsList = handler.getGunsList();
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      ex.printStackTrace();
    }
    return gunsList;
  }

  private static class AdvancedXMLHandler extends DefaultHandler {
    public List<Gun> guns = new ArrayList<>();
    private Gun gun;
    private String lastElementName;
    private TTC ttc;
    private RangeParameter rangeParameter;

    public List<Gun> getGunsList() {
      return guns;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
        throws SAXException {
      lastElementName = qName;
      if (!lastElementName.isEmpty()) {
        if (qName.equals("gun")) {
          String id = attributes.getValue("id");
          gun = new Gun();
          gun.setId(Integer.parseInt(id));
        }
        if (qName.equals("ttc")) ttc = new TTC();
        if (qName.equals("rangeParameters")) rangeParameter = new RangeParameter();
      }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
      String information = new String(ch, start, length);

      information = information.replace("\n", "").trim();

      if (!information.isEmpty()) {
        if (lastElementName.equals("model")) gun.setModel(information);
        if (lastElementName.equals("handy")) gun.setHandy(Integer.parseInt(information));
        if (lastElementName.equals("origin")) gun.setOrigin(information);
        if (lastElementName.equals("maximumFiringRange"))
          rangeParameter.setMaximumFiringRange(Integer.parseInt(information));
        if (lastElementName.equals("distanceRange"))
          rangeParameter.setDistanceRange(DistanceRange.valueOf(information.toUpperCase()));
        if (lastElementName.equals("effectiveFiringRange"))
          rangeParameter.setEffectiveFiringRange(Integer.parseInt(information));
        if (lastElementName.equals("hasMagazine"))
          ttc.setHasMagazine(Boolean.parseBoolean(information));
        if (lastElementName.equals("hasTelescopicSight"))
          ttc.setHasTelescopicSight(Boolean.parseBoolean(information));
        if (lastElementName.equals("material")) gun.setMaterial(information);
      }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

      if (!qName.isEmpty()) {
        if (qName.equals("ttc")) {
          gun.setTtc(ttc);
          ttc = null;
        }
        if (qName.equals("rangeParameters")) {
          ttc.setRangeParameter(rangeParameter);
          rangeParameter = null;
        }
        if (qName.equals("gun")) {
          guns.add(gun);
          gun = null;
        }
      }
    }
  }
}
