package XMLConvertor;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XmlToHtmlConverter {

  public static void convertXMLToHTML(File xml, File xslt) {
    StringWriter sw = new StringWriter();
    try {
      FileWriter fw = new FileWriter("src\\main\\resources\\gunXML.html");
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer(new StreamSource(xslt));
      transformer.transform(new StreamSource(xml), new StreamResult(sw));
      fw.write(sw.toString());
      fw.close();
      System.out.println("HTML file was generated successfully");
    } catch (IOException | TransformerException e) {
      e.printStackTrace();
    }
  }
}
