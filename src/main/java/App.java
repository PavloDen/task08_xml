import Comparator.GunComparatorById;
import Model.Gun;
import Parser.MyDOMParser;
import Parser.MySAXParser;
import Parser.MyStAXParser;
import XMLConvertor.XmlToHtmlConverter;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class App {
  public static void main(String[] args) {
    File xmlFile = new File("src\\main\\resources\\gunXML.xml");
    File xsdFile = new File("src\\main\\resources\\gunXSD.xsd");

    System.out.println("SAX parser was used");
    List<Gun> gunsSAX = new MySAXParser().parseGuns(xmlFile, xsdFile);
    System.out.println(gunsSAX);

    MyDOMParser myDOMParser = new MyDOMParser();
    List<Gun> gunsDOM = myDOMParser.parseGuns(xmlFile);
    System.out.println(gunsDOM);
    System.out.println("DOM parser was used");

    MyStAXParser staxParser = new MyStAXParser();
    List<Gun> gunsStAX = staxParser.parseGuns(xmlFile);
    System.out.println(gunsStAX);
    System.out.println("StAX parser was used");

    Collections.sort(gunsSAX, new GunComparatorById());
    System.out.println("List of Gun sorted by iD");
    System.out.println(gunsSAX);

    File xsltFile = new File("src\\main\\resources\\gunXSLT.xsl");
    XmlToHtmlConverter.convertXMLToHTML(xmlFile, xsltFile);
  }
}
