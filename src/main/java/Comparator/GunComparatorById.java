package Comparator;

import Model.Gun;

import java.util.Comparator;

public class GunComparatorById implements Comparator<Gun> {
  @Override
  public int compare(Gun o1, Gun o2) {
    return (int) (o1.getId() - o2.getId());
  }
}
