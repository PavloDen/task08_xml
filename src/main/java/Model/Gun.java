package Model;

public class Gun {
  private long id;
  private String model;
  private int handy;
  private String origin;
  private TTC ttc;
  private String material;

  public Gun() {}

  public Gun(long id, String model, int handy, String origin, TTC ttc, String material) {
    this.id = id;
    this.model = model;
    this.handy = handy;
    this.origin = origin;
    this.ttc = ttc;
    this.material = material;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public int getHandy() {
    return handy;
  }

  public void setHandy(int handy) {
    this.handy = handy;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public TTC getTtc() {
    return ttc;
  }

  public void setTtc(TTC ttc) {
    this.ttc = ttc;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  @Override
  public String toString() {
    return "Gun{"
        + "id="
        + id
        + ", model='"
        + model
        + '\''
        + ", handy="
        + handy
        + ", origin='"
        + origin
        + '\''
        + ", ttc="
        + ttc
        + ", material='"
        + material
        + '\''
        + '}'
        + '\n';
  }
}
